<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="nav">
    <p class="subtitle">Navigation:</p>
    <div class="list">
        <a class="line-cell" href="user">Add New User</a><br />
        <a class="line-cell" href="users">List of users</a><br />
        <a class="line-cell" href="journals">Journal</a><br />
        <a class="line-cell" href="semesters">List of semesters</a><br />
        <a class="line-cell" href="subjects">List of subjects</a><br />
    </div>
</div>