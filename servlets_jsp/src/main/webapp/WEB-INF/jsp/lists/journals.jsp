<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:include page="../common/header.jsp">
    <jsp:param name="pageTitle" value="Journal's page"/>
</jsp:include>

<div class="container">
    <p class="subtitle">Journal:</p>
    <div class="list">
        <p class="small-cell">ID</p>
        <p class="journal-cell">STUDENT</p>
        <p class="journal-cell">SUBJECT</p>
        <p class="journal-cell">GRADE</p>
        <p class="journal-cell">GRADE DATE</p>
    </div>
    <c:forEach var="entry" items="${journal_entries}">
        <c:if test="${not empty entry.student}">
        <div class="list">
            <p class="small-cell"><c:out value="${entry.id}"/></p>
            <p class="journal-cell"><c:out value="${entry.student.firstName} ${entry.student.lastName}"/></p>
            <p class="journal-cell"><c:out value="${entry.subject.subjectName}"/></p>
            <p class="journal-cell"><c:out value="${entry.grade.value}"/></p>
            <p class="journal-cell"><c:out value="${entry.gradeDate}"/></p>
        </div>
        </c:if>
    </c:forEach>
</div>
<%@include file="../common/navigation.jsp"%>
<%@include file="../common/footer.jsp"%>
