<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:include page="../common/header.jsp">
    <jsp:param name="pageTitle" value="User's list page"/>
</jsp:include>

<div class="container">
    <p class="subtitle">User list:</p>
    <p class="pretext">modify or remove user profile:</p>
    <div class="list">
        <p class="small-cell">ID</p>
        <p class="user-cell">PERSON</p>
        <p class="user-cell">ROLE</p>
        <p class="user-cell">MODIFY</p>
        <p class="user-cell">REMOVE</p>
    </div>
    <c:forEach var="entry" items="${users}">
        <div class="list">
            <p class="small-cell"><c:out value="${entry.id}"/></p>
            <p class="user-cell"><c:out value="${entry.firstName} ${entry.lastName}"/></p>
            <p class="user-cell"><c:out value="${entry.role.name}"/></p>
            <a class="user-cell" href = "user?userId=<c:out value="${entry.id}"/>">update</a>
            <a class="user-cell" href = "delete?userId=<c:out value="${entry.id}"/>">delete</a>
        </div>
    </c:forEach>
</div>
<%@include file="../common/navigation.jsp"%>
<%@include file="../common/footer.jsp"%>