<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:include page="../common/header.jsp">
    <jsp:param name="pageTitle" value="Semester's page"/>
</jsp:include>

<div class="container">
    <p class="subtitle">Semesters list:</p>
    <div class="list">
        <p class="small-cell">NUMBER</p>
        <p class="semester-cell">START DATE</p>
        <p class="semester-cell">END DATE</p>
    </div>
    <c:forEach var="entry" items="${semesters}">
        <div class="list">
            <p class="small-cell"><c:out value="${entry.semesterNumber}"/></p>
            <p class="semester-cell"><c:out value="${entry.startDate}"/></p>
            <p class="semester-cell"><c:out value="${entry.endDate}"/></p>
        </div>
    </c:forEach>
</div>
<%@include file="../common/navigation.jsp"%>
<%@include file="../common/footer.jsp"%>