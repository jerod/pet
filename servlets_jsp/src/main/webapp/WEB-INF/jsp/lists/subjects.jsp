<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:include page="../common/header.jsp">
    <jsp:param name="pageTitle" value="Subject's page"/>
</jsp:include>

<div class="container">
    <p class="subtitle">Subject list:</p>
    <div class="list">
        <p class="small-cell">ID</p>
        <p class="subject-cell">LECTURER</p>
        <p class="subject-cell">SUBJECT</p>
        <p class="small-cell">SEMESTER</p>
    </div>
    <c:forEach var="entry" items="${subjects}">
        <div class="list">
            <p class="small-cell"><c:out value="${entry.id}"/></p>
            <p class="subject-cell"><c:out value="${entry.lecturer.firstName} ${entry.lecturer.lastName}"/></p>
            <p class="subject-cell"><c:out value="${entry.subjectName}"/></p>
            <p class="small-cell"><c:out value="${entry.semester.semesterNumber}"/></p>
        </div>
    </c:forEach>
</div>
<%@include file="../common/navigation.jsp"%>
<%@include file="../common/footer.jsp"%>