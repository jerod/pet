<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:include page="../common/header.jsp">
    <jsp:param name="pageTitle" value="User's edit page"/>
</jsp:include>

<div class="container">
    <p class="subtitle">Profile</p>
    <form class="form" action="user" method="POST">
        <c:if test="${not empty userToEdit.id}">
            <input type="hidden" name="person_id" value="<c:out value="${userToEdit.id}"/>">
        </c:if>
        <label class = "text">Name*: </label>
        <input name = "first_name" value="<c:out value="${userToEdit.firstName}"/>" required/>
        <label class = "text">Middlename: </label>
        <input name = "middle_name" value="<c:out value="${userToEdit.middleName}"/>"/>
        <label class = "text">Lastname*: </label>
        <input name = "last_name" value="<c:out value="${userToEdit.lastName}"/>" required/>
        <label class = "text">Login*: </label>
        <input name = "login" value="<c:out value="${userToEdit.login}"/>" required/>
        <label class = "text">Password*: </label>
        <input name = "password" value="<c:out value="${userToEdit.password}"/>" required/>
        <label class = "text">Phone number: </label>
        <input name = "phone_number" value="<c:out value="${userToEdit.phoneNumber}"/>" />
        <label class = "text">Birthday: </label>
        <input name = "birth_date" type="date"
               value="<c:out value="${userToEdit.birthday}"/>"/>
        <label class = "text">Enrollment date: </label>
        <input name = "enrollment_date" type="date"
               value="<c:out value="${userToEdit.enrollmentDate}"/>"/>
        <label class="text">Status*: </label>
        <select name = "status_id">
            <c:forEach var="status" items="${statuses}">
                <option value="<c:out value="${status.statusId}"/>"
                 <c:if test="${status.statusId eq userToEdit.status.statusId}">
                     selected
                 </c:if>
                >
                    <c:out value="${status.value}"/>
                </option>
            </c:forEach>
        </select>
        <label class="text">Role*: </label>
        <select name = "role_id">
            <c:forEach var="student" items="${roles}">
                <option value="<c:out value="${student.id}"/>"
                        <c:if test="${student.id eq userToEdit.role.id}">
                            selected
                        </c:if>
                >
                    <c:out value="${student.name}"/>
                </option>
            </c:forEach>
        </select>
        <label class="text">Semester*: </label>
        <select name = "current_semester_id">
            <c:forEach var="entry" items="${semesters}">
                <option value="<c:out value="${entry.id}"/>"
                        <c:if test="${entry.id eq userToEdit.currentSemester.id}">
                            selected
                        </c:if>
                >
                    <c:out value="${entry.semesterNumber}"/>
                </option>
            </c:forEach>
        </select>
        <c:choose>
            <c:when test="${empty userToEdit}">
                <a href="user" class="btn">Cancel</a>
            </c:when>
            <c:when test="${not empty userToEdit}">
                <a href="user?userId=<c:out value="${userToEdit.id}"/>" class="btn">Cancel</a>
            </c:when>
        </c:choose>
        <button class="btn">Ok</button>
    </form>
</div>
<%@include file="../common/navigation.jsp"%>
<%@include file="../common/footer.jsp"%>