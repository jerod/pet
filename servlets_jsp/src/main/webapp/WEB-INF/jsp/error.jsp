<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:include page="common/header.jsp">
    <jsp:param name="pageTitle" value="Error page"/>
</jsp:include>

<div class="container">
    <p class="subtitle">
        <c:out value="${requestScope['javax.servlet.error.error_code']}"/> Error occurred!
    </p>
    <div class="plain_block">
        <p class="text"><c:out value="${requestScope['javax.servlet.error.message']}"/></p>
    </div>
    <a class="btn" href="<c:out value="${redirect_link}"/>">back</a>
</div>
<%@include file="common/footer.jsp" %>