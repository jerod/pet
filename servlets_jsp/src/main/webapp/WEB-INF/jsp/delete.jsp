<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:include page="common/header.jsp">
    <jsp:param name="pageTitle" value="Delete user"/>
</jsp:include>

<div class="container">
    <form action="delete" class="form" method="POST">
        <input type="hidden" name="person_id" value="<c:out value="${userToDelete.id}"/>">
        <p class="text">Are you sure?</p>
        <button name="decision" value="false" class="btn">No</button>
        <button name="decision" value="true" class="btn">Yes</button>
    </form>
</div>
<%@include file="common/footer.jsp"%>
