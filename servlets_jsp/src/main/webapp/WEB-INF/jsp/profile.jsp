<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:include page="common/header.jsp">
    <jsp:param name="pageTitle" value="Profile"/>
</jsp:include>

<div class="container">
    <p class="subtitle">Hello, <c:out value="${user.login}"/></p>
    <p class="pretext">Your account info:</p>
    <p class="text">Name: <c:out value="${user.firstName}"/></p>
    <p class="text">Surname: <c:out value="${user.lastName}"/></p>
    <p class="text">Role: <c:out value="${user.role.name}"/></p>
    <p class="text">Status: <c:out value="${user.status.value}"/></p>
</div>
<c:if test="${user.role.name eq 'admin'}">
    <%@include file="common/navigation.jsp"%>
</c:if>
<%@include file="common/footer.jsp"%>