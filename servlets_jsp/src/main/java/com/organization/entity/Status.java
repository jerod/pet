package com.organization.entity;

public class Status {

    private Long statusId;
    private String value;
    private String description;

    public Status() {
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Status{" +
                "statusId=" + statusId +
                ", value='" + value + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
