package com.organization.entity;

public class Subject {

    private Long id;
    private String subjectName;
    private User lecturer;
    private Semester semester;

    public Subject() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public User getLecturer() {
        return lecturer;
    }

    public void setLecturer(User lecturer) {
        this.lecturer = lecturer;
    }

    public Semester getSemester() {
        return semester;
    }

    public void setSemester(Semester semester) {
        this.semester = semester;
    }

    @Override
    public String toString() {
        return "Subject{" +
                "subjectId=" + id +
                ", subjectName='" + subjectName + '\'' +
                ", lecturer=" + lecturer +
                ", semester=" + semester +
                '}';
    }
}
