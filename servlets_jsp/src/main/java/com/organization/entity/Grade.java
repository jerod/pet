package com.organization.entity;

public class Grade {

    private Long id;
    private Long value;
    private String description;

    public Grade() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Grade{" +
                "gradeId=" + id +
                ", value=" + value +
                ", description='" + description + '\'' +
                '}';
    }
}
