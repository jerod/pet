package com.organization.entity;

import java.sql.Date;

public class Semester {

    private Long id;
    private Long semesterNumber;
    private Date startDate;
    private Date endDate;

    public Semester() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSemesterNumber() {
        return semesterNumber;
    }

    public void setSemesterNumber(Long semesterNumber) {
        this.semesterNumber = semesterNumber;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "Semester{" +
                "semesterId=" + id +
                ", semesterNumber=" + semesterNumber +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }

}
