package com.organization.dao.api;

import com.organization.entity.Semester;

import java.util.List;

/**
 * Provides CRUD methods for working with DB, that contains table SEMESTER with
 * expected column names.
 *
 * @author Dmytro Vasylenko
 * @since 1.0
 */
public interface SemesterDao {

    /**
     * Adds Semester to database.
     *
     * @param semester   Semester for adding to DB.
     */
    void create(Semester semester);

    /**
     * Updates existing Semester in DB. If Semester doesn't contain in DB, then
     * DB isn't changed.
     *
     * @param semester   Semester for updating.
     */
    void update(Semester semester);

    /**
     * Removes Semesters from DB if it exists. If Semester doesn't contain in DB,
     * then DB isn't changed.
     *
     * @param semester   Semester for removing.
     */
    void remove(Semester semester);

    /**
     * Finds all Semesters in DB and returns it in container, that implements
     * interface List.
     *
     * @return List with semesters.
     */
    List<Semester> findAll();

    /**
     * Finds Semester in DB by id and returns it if exists. Otherwise returns
     * null.
     *
     * @param id    Semester's id, for searching in DB.
     * @return      Object of class Semester.
     */
    Semester findById(Long id);
}
