package com.organization.dao.api;

import com.organization.entity.Journal;

import java.util.List;

/**
 * Provides CRUD methods for working with DB, that contains table JOURNAL with
 * expected column names.
 *
 * @author Dmytro Vasylenko
 * @since 1.0
 */
public interface JournalDao {

    /**
     * Adds Journal to database.
     *
     * @param journal   Journal for adding to DB.
     */
    void create(Journal journal);

    /**
     * Updates existing Journal in DB. If Journal doesn't contain in DB, then
     * DB isn't changed.
     *
     * @param journal   Journal for updating.
     */
    void update(Journal journal);

    /**
     * Removes Journals from DB if it exists. If Journal doesn't contain in DB,
     * then DB isn't changed.
     *
     * @param journal   Journal for removing.
     */
    void remove(Journal journal);

    /**
     * Finds all Journals in DB and returns it in container, that implements
     * interface List.
     *
     * @return List with journals.
     */
    List<Journal> findAll();

	/**
	 * Finds Journal in DB by id and returns it if exists. Otherwise returns
	 * null.
	 *
	 * @param id    Journal's id, for searching in DB.
	 * @return      Object of class Journal.
	 */
	Journal findById(Long id);
}
