package com.organization.dao.api;

import com.organization.entity.Subject;

import java.util.List;

/**
 * Provides CRUD methods for working with DB, that contains table SUBJECT with
 * expected column names.
 *
 * @author Dmytro Vasylenko
 * @since 1.0
 */
public interface SubjectDao {

    /**
     * Adds Subject to database.
     *
     * @param subject   Subject for adding to DB.
     */
    void create(Subject subject);

    /**
     * Updates existing Subject in DB. If Subject doesn't contain in DB, then
     * DB isn't changed.
     *
     * @param subject   Subject for updating.
     */
    void update(Subject subject);

    /**
     * Removes Subjects from DB if it exists. If Subject doesn't contain in DB,
     * then DB isn't changed.
     *
     * @param subject   Subject for removing.
     */
    void remove(Subject subject);

    /**
     * Finds all Subjects in DB and returns it in container, that implements
     * interface List.
     *
     * @return List with subjects.
     */
    List<Subject> findAll();

	/**
	 * Finds Subject in DB by id and returns it if exists. Otherwise returns
	 * null.
	 *
	 * @param id    Subject's id, for searching in DB.
	 * @return      Object of class Subject.
	 */
	Subject findById(Long id);
}