package com.organization.dao.api;

import com.organization.entity.Status;

import java.util.List;

/**
 * Provides CRUD methods for working with DB, that contains table STATUS with
 * expected column names.
 *
 * @author Dmytro Vasylenko
 * @since 1.0
 */
public interface StatusDao {

    /**
     * Adds Status to database.
     *
     * @param status   Status for adding to DB.
     */
    void create(Status status);

    /**
     * Updates existing Status in DB. If Status doesn't contain in DB, then DB
     * isn't changed.
     *
     * @param status   Status for updating.
     */
    void update(Status status);

    /**
     * Removes Statuses from DB if it exists. If Status doesn't contain in DB,
     * then DB isn't changed.
     *
     * @param status   Status for removing.
     */
    void remove(Status status);

    /**
     * Finds all Statuses in DB and returns it in container, that implements
     * interface List.
     *
     * @return List with statuses.
     */
    List<Status> findAll();

    /**
     * Finds Status in DB by id and returns it if exists. Otherwise returns
     * null.
     *
     * @param id    Status's id, for searching in DB.
     * @return      Object of class Status.
     */
    Status findById(Long id);
}
