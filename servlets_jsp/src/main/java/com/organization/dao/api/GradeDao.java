package com.organization.dao.api;

import com.organization.entity.Grade;

import java.util.List;

/**
 * Provides CRUD methods for working with DB, that contains table GRADE with
 * expected column names.
 *
 * @author Dmytro Vasylenko
 * @since 1.0
 */
public interface GradeDao {

    /**
     * Adds Grade to database.
     *
     * @param grade   Grade for adding to DB.
     */
    void create(Grade grade);

    /**
     * Updates existing Grade in DB. If Grade doesn't contain in DB, then
     * DB isn't changed.
     *
     * @param grade   Grade for updating.
     */
    void update(Grade grade);

    /**
     * Removes Grades from DB if it exists. If Grade doesn't contain in DB,
     * then DB isn't changed.
     *
     * @param grade   Grade for removing.
     */
    void remove(Grade grade);

    /**
     * Finds all Grades in DB and returns it in container, that implements
     * interface List.
     *
     * @return List with grades.
     */
    List<Grade> findAll();

	/**
	 * Finds Grade in DB by id and returns it if exists. Otherwise returns
	 * null.
	 *
	 * @param id    Grade's id, for searching in DB.
	 * @return      Object of class Grade.
	 */
	Grade findById(Long id);
}
