package com.organization.dao.api;

import java.util.List;

import com.organization.entity.Role;

/**
 * Provides CRUD methods for working with DB, that contains table ROLE with
 * expected column names.
 *
 * @author Dmytro Vasylenko
 * @since 1.0
 */
public interface RoleDao {

    /**
     * Adds Role to database.
     *
     * @param role   Role for adding to DB.
     */
    void create(Role role);

    /**
     * Updates existing Role in DB. If Role doesn't contain in DB, then
     * DB isn't changed.
     *
     * @param role   Role for updating.
     */
    void update(Role role);

    /**
     * Removes Roles from DB if it exists. If Role doesn't contain in DB,
     * then DB isn't changed.
     *
     * @param role   Role for removing.
     */
    void remove(Role role);

    /**
     * Finds all Roles in DB and returns it in container, that implements
     * interface List.
     *
     * @return List with roles.
     */
    List<Role> findAll();

    /**
     * Finds Role in DB by id and returns it if exists. Otherwise returns
     * null.
     *
     * @param id    Role's id, for searching in DB.
     * @return      Object of class Role.
     */
    Role findById(Long id);
}