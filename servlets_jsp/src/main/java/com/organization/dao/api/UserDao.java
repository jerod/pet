package com.organization.dao.api;

import java.util.List;

import com.organization.entity.User;

/**
 * Provides CRUD methods for working with DB, that contains table PERSON with
 * expected column names.
 *
 * @author Dmytro Vasylenko
 * @since 1.0
 */
public interface UserDao {

    /**
     * Adds User to database.
     *
     * @param user User for adding to DB.
     */
    void create(User user);

    /**
     * Updates existing User in DB. If User doesn't contain in DB, then DB isn't
     * changed.
     *
     * @param user User for updating.
     */
    void update(User user);

    /**
     * Removes User from DB if it exists. If User doesn't contain in DB, then DB
     * isn't changed.
     *
     * @param user User for removing.
     */
    void remove(User user);

    /**
     * Finds all Users in DB and returns it in container, that implements
     * interface List.
     *
     * @return List with users.
     */
    List<User> findAll();

    /**
     * Finds User in DB by login and returns it if exists. Otherwise returns
     * null.
     *
     * @param login Users login, for searching in DB.
     * @return      Object of class User.
     */
    User findByLogin(String login);

    /**
     * Finds User in DB by id and returns it if exists. Otherwise returns
     * null.
     *
     * @param id    User's id, for searching in DB.
     * @return      Object of class User.
     */
    User findById(Long id);
}
