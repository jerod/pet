package com.organization.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.organization.dao.DaoFactory;
import com.organization.dao.api.UserDao;
import com.organization.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class UserDaoImpl implements UserDao {

    private static final Logger LOG = LogManager.getLogger();
    private static final String CREATE_SQL =
            "INSERT INTO person(first_name, middle_name, last_name, birthday, "
                    + "phone_number, enrollment_date, login, password, "
                    + "status_id, current_semester_id, role_id) "
                    + "VALUES(?,?,?,?,?,?,?,?,?,?,?)";
    private static final String UPDATE_SQL =
            "UPDATE person SET first_name = ?, middle_name = ?, last_name = ?, "
                    + "birthday = ?, phone_number = ?, enrollment_date = ?, "
                    + "password = ?, status_id = ?, current_semester_id = ?, "
                    + "role_id = ? WHERE login = ?";
    private static final String REMOVE_SQL =
            "DELETE FROM person WHERE id = ?";
    public static final String FIND_BY_ID_SQL =
            "SELECT * FROM person WHERE id = ?";
    private static final String ALL_USERS_SQL =
            "SELECT * FROM person";
    public static final String FIND_BY_LOGIN_SQL =
            "SELECT * FROM person WHERE login = ?";

    private final DaoFactory factory;

    public UserDaoImpl(DaoFactory factory) {
        this.factory = factory;
    }

    @Override
    public void create(User user) {

        LOG.trace("Start create(User user)");

        try (Connection connection = factory.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(CREATE_SQL)) {
                ps.setString(1, user.getFirstName());
                ps.setString(2, user.getMiddleName());
                ps.setString(3, user.getLastName());
                ps.setDate(4, user.getBirthday());
                ps.setString(5, user.getPhoneNumber());
                ps.setDate(6, user.getEnrollmentDate());
                ps.setString(7, user.getLogin());
                ps.setString(8, user.getPassword());
                ps.setLong(9, user.getStatus().getStatusId());
                ps.setLong(10, user.getCurrentSemester().getId());
                ps.setLong(11, user.getRole().getId());
                ps.execute();
                connection.commit();

                LOG.trace("create(User user). Transaction executed");
            } catch (SQLException e) {
                connection.rollback();
                LOG.catching(e);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        LOG.trace("End create(User user)");
    }

    @Override
    public void update(User user) {

        LOG.trace("Start update(User user)");

        try (Connection connection = factory.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(UPDATE_SQL)) {
                ps.setString(1, user.getFirstName());
                ps.setString(2, user.getMiddleName());
                ps.setString(3, user.getLastName());
                ps.setDate(4, user.getBirthday());
                ps.setString(5, user.getPhoneNumber());
                ps.setDate(6, user.getEnrollmentDate());
                ps.setString(7, user.getPassword());
                ps.setLong(8, user.getStatus().getStatusId());
                ps.setLong(9, user.getRole().getId());
                ps.setLong(10, user.getCurrentSemester().getId());
                ps.setString(11, user.getLogin());
                ps.executeUpdate();
                connection.commit();

                LOG.trace("update(User user). Transaction executed");
            } catch (SQLException e) {
                connection.rollback();
                LOG.throwing(new SQLException("Transaction failed", e));
            }
        } catch (SQLException e) {
            LOG.catching(e);
        }
        LOG.trace("End update(User user)");
    }

    @Override
    public void remove(User user) {

        LOG.trace("Start remove(User user)");

        try (Connection connection = factory.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(REMOVE_SQL)) {
                ps.setLong(1, user.getId());
                ps.executeUpdate();
                connection.commit();

                LOG.trace("remove(User user). Transaction executed");
            } catch (SQLException e) {
                connection.rollback();
                LOG.throwing(new SQLException("Transaction failed", e));
            }
        } catch (SQLException e) {
            LOG.catching(e);
        }
        LOG.trace("End remove(User user)");
    }

    @Override
    public List<User> findAll() {

        LOG.trace("Start findAll()");

        List<User> users = new ArrayList<>();
        User user;
        try (Connection connection = factory.getConnection()) {
            try (Statement stmt = connection.createStatement()) {
                try (ResultSet rs = stmt.executeQuery(ALL_USERS_SQL)) {
                    while (rs.next()) {
                        user = new User();
                        user.setId(rs.getLong(1));
                        user.setFirstName(rs.getString(2));
                        user.setMiddleName(rs.getString(3));
                        user.setLastName(rs.getString(4));
                        user.setBirthday(rs.getDate(5));
                        user.setPhoneNumber(rs.getString(6));
                        user.setEnrollmentDate(rs.getDate(7));
                        user.setLogin(rs.getString(8));
                        user.setPassword(rs.getString(9));
                        user.setStatus(factory.getStatusDao()
                                .findById(rs.getLong(10)));
                        user.setCurrentSemester(factory.getSemesterDao()
                                .findById(rs.getLong(11)));
                        user.setRole(factory.getRoleDao()
                                .findById(rs.getLong(12)));

                        users.add(user);
                    }
                }
            } catch (SQLException e) {
                LOG.throwing(new SQLException("Cannot find all users in DB", e));
            }
        } catch (SQLException e) {
            LOG.catching(e);
        }
        LOG.trace("End findAll()");

        return users;
    }

    @Override
    public User findByLogin(String login) {

        LOG.trace("Start findByLogin(String login)");

        User user = null;
        try (Connection connection = factory.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(FIND_BY_LOGIN_SQL)) {
                ps.setString(1, login);
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        user = new User();
                        user.setId(rs.getLong(1));
                        user.setFirstName(rs.getString(2));
                        user.setMiddleName(rs.getString(3));
                        user.setLastName(rs.getString(4));
                        user.setBirthday(rs.getDate(5));
                        user.setPhoneNumber(rs.getString(6));
                        user.setEnrollmentDate(rs.getDate(7));
                        user.setLogin(rs.getString(8));
                        user.setPassword(rs.getString(9));
                        user.setStatus(factory.getStatusDao()
                                .findById(rs.getLong(10)));
                        user.setCurrentSemester(factory.getSemesterDao()
                                .findById(rs.getLong(11)));
                        user.setRole(factory.getRoleDao()
                                .findById(rs.getLong(12)));
                    }
                }
            } catch (SQLException e) {
                LOG.throwing(new SQLException("Cannot find this user in DB", e));
            }
        } catch (SQLException e) {
            LOG.catching(e);
        }
        LOG.trace("End findBy(String columnName, String value)");
        return user;
    }

    @Override
    public User findById(Long id) {

        LOG.trace("Start findById(Long id)");

        User user = null;
        try (Connection connection = factory.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID_SQL)) {
                ps.setLong(1, id);
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        user = new User();
                        user.setId(rs.getLong(1));
                        user.setFirstName(rs.getString(2));
                        user.setMiddleName(rs.getString(3));
                        user.setLastName(rs.getString(4));
                        user.setBirthday(rs.getDate(5));
                        user.setPhoneNumber(rs.getString(6));
                        user.setEnrollmentDate(rs.getDate(7));
                        user.setLogin(rs.getString(8));
                        user.setPassword(rs.getString(9));
                        user.setStatus(factory.getStatusDao()
                                .findById(rs.getLong(10)));
                        user.setCurrentSemester(factory.getSemesterDao()
                                .findById(rs.getLong(11)));
                        user.setRole(factory.getRoleDao()
                                .findById(rs.getLong(12)));
                    }
                }
            } catch (SQLException e) {
                LOG.throwing(new SQLException("Cannot find this user in DB", e));
            }
        } catch (SQLException e) {
            LOG.catching(e);
        }
        LOG.trace("End findById(Long id)");
        return user;
    }
}