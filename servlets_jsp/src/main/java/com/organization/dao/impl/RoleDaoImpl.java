package com.organization.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.organization.dao.DaoFactory;
import com.organization.dao.api.RoleDao;
import com.organization.entity.Role;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Implements all methods of interface RoleDao for creating, updating, deleting
 * and finding values in table ROLE in DB. Provides access to DB using JDBC API.
 *
 * @author Dmytro Vasylenko
 * @since 1.0
 */
public class RoleDaoImpl implements RoleDao {

    private static final String CREATE_SQL =
            "INSERT INTO role(name, description) VALUES(?,?)";
    private static final String UPDATE_SQL =
            "UPDATE role SET name = ?, description = ? WHERE id = ?";
    private static final String REMOVE_SQL =
            "DELETE FROM role WHERE id = ?";
    private static final String FIND_ALL_SQL = "SELECT * FROM role";
    private static final String FIND_BY_ID_SQL =
            "SELECT * FROM role WHERE id = ?";
    private static final Logger LOG = LogManager.getLogger();

    private final DaoFactory factory;

    public RoleDaoImpl(DaoFactory factory) {
        this.factory = factory;
    }

    @Override
    public void create(Role role) {

        LOG.trace("Start create(Role role)");

        try (Connection connection = factory.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(CREATE_SQL)) {
                ps.setString(1, role.getName());
                ps.setString(2, role.getDescription());
                ps.execute();
                connection.commit();

                LOG.trace("create(Role role). Transaction executed");
            } catch (SQLException e) {
                connection.rollback();
                LOG.throwing(new SQLException("Transaction failed", e));
            }
        } catch (SQLException e) {
            LOG.catching(e);
        }
        LOG.trace("End create(Role role)");
    }

    @Override
    public void update(Role role) {

        LOG.trace("Start update(Role role)");

        try (Connection connection = factory.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(UPDATE_SQL)) {
                ps.setString(1, role.getName());
                ps.setString(2, role.getDescription());
                ps.setLong(3, role.getId());
                ps.executeUpdate();
                connection.commit();

                LOG.trace("update(Role role). Transaction executed");
            } catch (SQLException e) {
                connection.rollback();
                LOG.throwing(new SQLException("Transaction failed", e));
            }
        } catch (SQLException e) {
            LOG.catching(e);
        }
        LOG.trace("End update(Role role)");
    }

    @Override
    public void remove(Role role) {

        LOG.trace("Start remove(Role role)");

        try (Connection connection = factory.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(REMOVE_SQL)) {
                ps.setLong(1, role.getId());
                ps.executeUpdate();
                connection.commit();

                LOG.trace("remove(Role role). Transaction executed");
            } catch (SQLException e) {
                connection.rollback();
                LOG.throwing(new SQLException("Transaction failed", e));
            }
        } catch (SQLException e) {
            LOG.catching(e);
        }
        LOG.trace("End remove(Role role)");
    }

    @Override
    public List<Role> findAll() {

        LOG.trace("Start findAll()");

        List<Role> roles = new ArrayList<>();
        Role role;
        try (Connection connection = factory.getConnection()) {
            try (Statement stmt = connection.createStatement()) {
                try (ResultSet rs = stmt.executeQuery(FIND_ALL_SQL)) {
                    while (rs.next()) {
                        role = new Role();
                        role.setId(rs.getLong(1));
                        role.setName(rs.getString(2));
                        role.setDescription(rs.getString(3));
                        roles.add(role);
                    }
                }
            } catch (SQLException e) {
                LOG.error("Exception: findAll(). Cannot find roles");
            }
        } catch (SQLException e) {
            LOG.catching(e);
        }
        LOG.trace("End findAll()");

        return roles;
    }

    @Override
    public Role findById(Long id) {

        LOG.trace("Start findById(Long id)");

        Role role = null;
        try (Connection connection = factory.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID_SQL)) {
                ps.setLong(1, id);
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        role = new Role();
                        role.setId(rs.getLong(1));
                        role.setName(rs.getString(2));
                        role.setDescription(rs.getString(3));
                    }
                }
            } catch (SQLException e) {
                LOG.throwing(new SQLException("Cannot find this role in DB", e));
            }
        } catch (SQLException e) {
            LOG.catching(e);
        }
        LOG.trace("End findById(Long id)");

        return role;
    }
}
