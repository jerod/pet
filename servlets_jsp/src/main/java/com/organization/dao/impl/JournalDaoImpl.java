package com.organization.dao.impl;

import com.organization.dao.DaoFactory;
import com.organization.dao.api.JournalDao;
import com.organization.entity.Journal;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Implements all methods of interface JournalDao for creating, updating, deleting
 * and finding values in table JOURNAL in DB. Provides access to DB using JDBC API.
 *
 * @author Dmytro Vasylenko
 * @since 1.0
 */
public class JournalDaoImpl implements JournalDao {

    private static final String CREATE_SQL = "INSERT INTO journal "
            + "(student_id, subject_id, grade_id, grade_date) VALUES (?,?,?,?)";
    private static final String UPDATE_SQL = "UPDATE journal SET "
            + "student_id = ?, subject_id = ?, grade_id = ?, grade_date = ? "
			+ "WHERE id = ?";
    private static final String REMOVE_SQL = "DELETE FROM journal WHERE id = ?";
    private static final String FIND_ALL_SQL = "SELECT * FROM journal";
	private static final String FIND_BY_ID_SQL =
			"SELECT * FROM journal WHERE id = ?";
	private static final Logger LOG = LogManager.getLogger();

	private final DaoFactory factory;

	public JournalDaoImpl(DaoFactory factory) {
		this.factory = factory;
	}

    @Override
    public void create(Journal journal) {

        LOG.trace("Start create(Journal journal)");

        try (Connection connection = factory.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(CREATE_SQL)) {
                ps.setLong(1, journal.getStudent().getId());
                ps.setLong(2, journal.getSubject().getId());
                ps.setLong(3, journal.getGrade().getId());
                ps.setDate(4, journal.getGradeDate());
                ps.executeUpdate();
                connection.commit();

                LOG.trace("create(Journal journal). Transaction executed");
            } catch (SQLException e) {
                connection.rollback();
                LOG.throwing(new SQLException("Transaction failed", e));
            }
        } catch (SQLException e) {
            LOG.catching(e);
        }
        LOG.trace("End create(Subject subject)");
    }

    @Override
    public void update(Journal journal) {

        LOG.trace("Start update(Journal journal)");

        try (Connection connection = factory.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(UPDATE_SQL)) {
                ps.setLong(1, journal.getStudent().getId());
                ps.setLong(2, journal.getSubject().getId());
                ps.setLong(3, journal.getGrade().getId());
                ps.setDate(4, journal.getGradeDate());
                ps.setLong(5, journal.getId());
                ps.executeUpdate();
                connection.commit();

                LOG.trace("update(Journal journal). Transaction executed");
            } catch (SQLException e) {
                connection.rollback();
                LOG.throwing(new SQLException("Transaction failed", e));
            }
        } catch (SQLException e) {
            LOG.catching(e);
        }
        LOG.trace("End update(Subject subject)");
    }

    @Override
    public void remove(Journal journal) {

        LOG.trace("Start remove(Journal journal)");

        try (Connection connection = factory.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(REMOVE_SQL)) {
                ps.setLong(1, journal.getId());
                ps.executeUpdate();
                connection.commit();

                LOG.trace("remove(Journal journal). Transaction executed");
            } catch (SQLException e) {
                connection.rollback();
                LOG.throwing(new SQLException("Transaction failed", e));
            }
        } catch (SQLException e) {
            LOG.catching(e);
        }
        LOG.trace("End remove(Journal journal)");
    }

    @Override
    public List<Journal> findAll() {

        LOG.trace("Start findAll()");

        List<Journal> journals = new ArrayList<>();
        Journal journal;
        try (Connection connection = factory.getConnection()) {
            try (Statement stmt = connection.createStatement()) {
                try (ResultSet rs = stmt.executeQuery(FIND_ALL_SQL)) {
                    while (rs.next()) {
                        journal = new Journal();
						journal.setId(rs.getLong(1));
                        journal.setStudent(
                        		factory.getUserDao().findById(rs.getLong(2)));
                        journal.setSubject(
                        		factory.getSubjectDao().findById(rs.getLong(3)));
                        journal.setGrade(
                        		factory.getGradeDao().findById(rs.getLong(4)));
						journal.setGradeDate(rs.getDate(5));
                        journals.add(journal);
                    }
                }
            } catch (SQLException e) {
                LOG.error("Exception: findAll(). Cannot find journals");
            }
        } catch (SQLException e) {
            LOG.catching(e);
        }
        LOG.trace("End findAll()");

        return journals;
    }

	@Override
	public Journal findById(Long id) {

		LOG.trace("Start findById(Long id)");

		Journal journal = null;
		try (Connection connection = factory.getConnection()) {
			try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID_SQL)) {
				ps.setLong(1, id);
				try (ResultSet rs = ps.executeQuery()) {
					if (rs.next()) {
						journal = new Journal();
						journal.setStudent(
								factory.getUserDao().findById(rs.getLong(1)));
						journal.setSubject(
								factory.getSubjectDao().findById(rs.getLong(2)));
						journal.setGrade(
								factory.getGradeDao().findById(rs.getLong(3)));
						journal.setGradeDate(rs.getDate(4));
					}
				}
			} catch (SQLException e) {
				LOG.throwing(new SQLException("Cannot find this journal in DB", e));
			}
		} catch (SQLException e) {
			LOG.catching(e);
		}
		LOG.trace("End findById(Long id)");

		return journal;
	}
}
