package com.organization.dao.impl;

import com.organization.dao.DaoFactory;
import com.organization.dao.api.StatusDao;
import com.organization.entity.Status;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Implements all methods of interface StatusDao for creating, updating, deleting
 * and finding values in table STATUS in DB. Provides access to DB using JDBC API.
 *
 * @author Dmytro Vasylenko
 * @since 1.0
 */
public class StatusDaoImpl implements StatusDao {

    private static final String CREATE_SQL = "INSERT INTO status "
            + "(value, description) VALUES (?,?)";
    private static final String UPDATE_SQL = "UPDATE status SET "
            + "value = ?, description = ? WHERE status_id = ?";
    private static final String REMOVE_SQL =
            "DELETE FROM status WHERE status_id = ?";
    private static final String FIND_ALL_SQL = "SELECT * FROM status";
    private static final String FIND_BY_ID_SQL =
            "SELECT * FROM status WHERE status_id = ?";
    private static final Logger LOG = LogManager.getLogger();

    private final DaoFactory factory;

    public StatusDaoImpl(DaoFactory factory) {
        this.factory = factory;
    }

    @Override
    public void create(Status status) {

        LOG.trace("Start create(Status status)");

        try (Connection connection = factory.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(CREATE_SQL)) {
                ps.setString(1, status.getValue());
                ps.setString(2, status.getDescription());
                ps.executeUpdate();
                connection.commit();

                LOG.trace("create(Status status). Transaction executed");
            } catch (SQLException e) {
                connection.rollback();
                LOG.throwing(new SQLException("Transaction failed", e));
            }
        } catch (SQLException e) {
            LOG.catching(e);
        }
        LOG.trace("End create(Status status)");
    }

    @Override
    public void update(Status status) {

        LOG.trace("Start update(Status status)");

        try (Connection connection = factory.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(UPDATE_SQL)) {
                ps.setString(1, status.getValue());
                ps.setString(2, status.getDescription());
                ps.setLong(3, status.getStatusId());
                ps.executeUpdate();
                connection.commit();

                LOG.trace("update(Status status). Transaction executed");
            } catch (SQLException e) {
                connection.rollback();
                LOG.throwing(new SQLException("Transaction failed", e));
            }
        } catch (SQLException e) {
            LOG.catching(e);
        }
        LOG.trace("End update(Status status)");
    }

    @Override
    public void remove(Status status) {

        LOG.trace("Start create(Status status)");

        try (Connection connection = factory.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(REMOVE_SQL)) {
                ps.setLong(1, status.getStatusId());
                ps.executeUpdate();
                connection.commit();

                LOG.trace("remove(Status status). Transaction executed");
            } catch (SQLException e) {
                connection.rollback();
                LOG.throwing(new SQLException("Transaction failed", e));
            }
        } catch (SQLException e) {
            LOG.catching(e);
        }
        LOG.trace("End remove(Status status)");
    }

    @Override
    public List<Status> findAll() {

        LOG.trace("Start findAll()");

        List<Status> statuses = new ArrayList<>();
        Status status;
        try (Connection connection = factory.getConnection()) {
            try (Statement stmt = connection.createStatement()) {
                try (ResultSet rs = stmt.executeQuery(FIND_ALL_SQL)) {
                    while (rs.next()) {
                        status = new Status();
                        status.setStatusId(rs.getLong(1));
                        status.setValue(rs.getString(2));
                        statuses.add(status);
                    }
                }
            } catch (SQLException e) {
                LOG.error("Exception: findAll(). Cannot find statuses");
            }
        } catch (SQLException e) {
            LOG.catching(e);
        }
        LOG.trace("End findAll()");

        return statuses;
    }

    @Override
    public Status findById(Long id) {

        LOG.trace("Start findById(Long id)");

        Status status = null;
        try (Connection connection = factory.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID_SQL)) {
                ps.setLong(1, id);
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        status = new Status();
                        status.setStatusId(rs.getLong(1));
                        status.setValue(rs.getString(2));
                    }
                }
            } catch (SQLException e) {
                LOG.throwing(new SQLException("Cannot find this status in DB", e));
            }
        } catch (SQLException e) {
            LOG.catching(e);
        }
        LOG.trace("End findById(Long id)");

        return status;
    }
}
