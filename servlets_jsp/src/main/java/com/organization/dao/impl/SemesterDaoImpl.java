package com.organization.dao.impl;

import com.organization.dao.DaoFactory;
import com.organization.dao.api.SemesterDao;
import com.organization.entity.Semester;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Implements all methods of interface SemesterDao for creating, updating, deleting
 * and finding values in table SEMESTER in DB. Provides access to DB using JDBC API.
 *
 * @author Dmytro Vasylenko
 * @since 1.0
 */
public class SemesterDaoImpl implements SemesterDao {

    private static final String CREATE_SQL = "INSERT INTO semester "
            + "(semester_number, start_date, end_date) VALUES (?,?,?)";
    private static final String UPDATE_SQL =
            "UPDATE semester SET semester_number = ?, start_date = ?, end_date = ? "
            + "WHERE semester_id = ?";
    private static final String REMOVE_SQL =
            "DELETE FROM semester WHERE semester_id = ?";
    private static final String FIND_ALL_SQL = "SELECT * FROM semester";
    private static final String FIND_BY_ID_SQL =
            "SELECT * FROM semester WHERE semester_id = ?";
    private static final Logger LOG = LogManager.getLogger();

    private final DaoFactory factory;

    public SemesterDaoImpl(DaoFactory factory) {
        this.factory = factory;
    }

    @Override
    public void create(Semester semester) {

        LOG.trace("Start create(Semester semester)");

        try (Connection connection = factory.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(CREATE_SQL)) {
                ps.setLong(1, semester.getSemesterNumber());
                ps.setDate(2, semester.getStartDate());
                ps.setDate(3, semester.getEndDate());
                ps.executeUpdate();
                connection.commit();

                LOG.trace("create(Semester semester). Transaction executed");
            } catch (SQLException e) {
                connection.rollback();
                LOG.throwing(new SQLException("Transaction failed", e));
            }
        } catch (SQLException e) {
            LOG.catching(e);
        }
        LOG.trace("End create(Semester semester)");
    }

    @Override
    public void update(Semester semester) {

        LOG.trace("Start update(Semester semester)");

        try (Connection connection = factory.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(UPDATE_SQL)) {
                ps.setLong(1, semester.getSemesterNumber());
                ps.setDate(2, semester.getStartDate());
                ps.setDate(3, semester.getEndDate());
                ps.setLong(4, semester.getId());
                ps.executeUpdate();
                connection.commit();

                LOG.trace("update(Semester semester). Transaction executed");
            } catch (SQLException e) {
                connection.rollback();
                LOG.throwing(new SQLException("Transaction failed", e));
            }
        } catch (SQLException e) {
            LOG.catching(e);
        }
        LOG.trace("End update(Semester semester)");
    }

    @Override
    public void remove(Semester semester) {

        LOG.trace("Start remove(Semester semester)");

        try (Connection connection = factory.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(REMOVE_SQL)) {
                ps.setLong(1, semester.getId());
                ps.executeUpdate();
                connection.commit();

                LOG.trace("remove(Semester semester). Transaction executed");
            } catch (SQLException e) {
                connection.rollback();
                LOG.throwing(new SQLException("Transaction failed", e));
            }
        } catch (SQLException e) {
            LOG.catching(e);
        }
        LOG.trace("End remove(Semester semester)");
    }

    @Override
    public List<Semester> findAll() {

        LOG.trace("Start findAll()");

        List<Semester> semesters = new ArrayList<>();
        Semester semester = null;
        try (Connection connection = factory.getConnection()) {
            try (Statement stmt = connection.createStatement()) {
                try (ResultSet rs = stmt.executeQuery(FIND_ALL_SQL)) {
                    while (rs.next()) {
                        semester = new Semester();
                        semester.setId(rs.getLong(1));
                        semester.setSemesterNumber(rs.getLong(2));
                        semester.setStartDate(rs.getDate(3));
                        semester.setEndDate(rs.getDate(4));
                        semesters.add(semester);
                    }
                }
            } catch (SQLException e) {
                LOG.error("Exception: findAll(). Cannot find semesters");
            }
        } catch (SQLException e) {
            LOG.catching(e);
        }
        LOG.trace("End findAll()");

        return semesters;
    }

    @Override
    public Semester findById(Long id) {

        LOG.trace("Start findById(Long id)");

        Semester semester = null;
        try (Connection connection = factory.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID_SQL)) {
                ps.setLong(1, id);
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        semester = new Semester();
                        semester.setId(rs.getLong(1));
                        semester.setSemesterNumber(rs.getLong(2));
                        semester.setStartDate(rs.getDate(3));
                        semester.setEndDate(rs.getDate(4));
                    }
                }
            } catch (SQLException e) {
                LOG.throwing(new SQLException("Cannot find this semester in DB", e));
            }
        } catch (SQLException e) {
            LOG.catching(e);
        }
        LOG.trace("End findById(Long id)");

        return semester;
    }
}
