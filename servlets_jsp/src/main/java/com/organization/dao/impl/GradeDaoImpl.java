package com.organization.dao.impl;

import com.organization.dao.DaoFactory;
import com.organization.dao.api.GradeDao;
import com.organization.entity.Grade;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Implements all methods of interface GradeDao for creating, updating, deleting
 * and finding values in table GRADE in DB. Provides access to DB using JDBC API.
 *
 * @author Dmytro Vasylenko
 * @since 1.0
 */
public class GradeDaoImpl implements GradeDao {

    private static final String CREATE_SQL =
            "INSERT INTO grade (value, description) VALUES (?,?)";
    private static final String UPDATE_SQL = "UPDATE grade SET "
            + "value = ?, description = ? WHERE grade_id = ?";
    private static final String REMOVE_SQL =
            "DELETE FROM grade WHERE grade_id = ?";
    private static final String FIND_ALL_SQL = "SELECT * FROM grade";
	private static final String FIND_BY_ID_SQL =
			"SELECT * FROM grade WHERE grade_id = ?";
    private static final Logger LOG = LogManager.getLogger();

	private final DaoFactory factory;

	public GradeDaoImpl(DaoFactory factory) {
		this.factory = factory;
	}


	@Override
    public void create(Grade grade) {

        LOG.trace("Start create(Grade grade)");

        try (Connection connection = factory.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(CREATE_SQL)) {
                ps.setLong(1, grade.getValue());
                ps.setString(2, grade.getDescription());
                ps.executeUpdate();
                connection.commit();

                LOG.trace("create(Grade grade). Transaction executed");
            } catch (SQLException e) {
                connection.rollback();
                LOG.throwing(new SQLException("Transaction failed", e));
            }
        } catch (SQLException e) {
            LOG.catching(e);
        }
        LOG.trace("End create(Grade grade)");
    }

    @Override
    public void update(Grade grade) {

        LOG.trace("Start update(Grade grade)");

        try (Connection connection = factory.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(UPDATE_SQL)) {
                ps.setLong(1, grade.getValue());
                ps.setString(2, grade.getDescription());
                ps.setLong(3, grade.getId());
                ps.executeUpdate();
                connection.commit();

                LOG.trace("update(Grade grade). Transaction executed");
            } catch (SQLException e) {
                connection.rollback();
                LOG.throwing(new SQLException("Transaction failed", e));
            }
        } catch (SQLException e) {
            LOG.catching(e);
        }
        LOG.trace("End update(Grade grade)");
    }

    @Override
    public void remove(Grade grade) {

        LOG.trace("Start remove(Grade grade)");

        try (Connection connection = factory.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(REMOVE_SQL)) {
                ps.setLong(1, grade.getId());
                ps.executeUpdate();
                connection.commit();

                LOG.trace("remove(Grade grade). Transaction executed");
            } catch (SQLException e) {
                connection.rollback();
                LOG.throwing(new SQLException("Transaction failed", e));
            }
        } catch (SQLException e) {
            LOG.catching(e);
        }
        LOG.trace("End remove(Grade grade)");
    }

    @Override
    public List<Grade> findAll() {

        LOG.trace("Start findAll()");

        List<Grade> grades = new ArrayList<>();
        Grade grade;
        try (Connection connection = factory.getConnection()) {
            try (Statement stmt = connection.createStatement()) {
                try (ResultSet rs = stmt.executeQuery(FIND_ALL_SQL)) {
                    while (rs.next()) {
                        grade = new Grade();
                        grade.setId(rs.getLong(1));
                        grade.setValue(rs.getLong(2));
                        grade.setDescription(rs.getString(3));
                        grades.add(grade);
                    }
                }
            } catch (SQLException e) {
                LOG.error("Exception: findAll(). Cannot find grades");
            }
        } catch (SQLException e) {
            LOG.catching(e);
        }
        LOG.trace("End findAll()");
        return grades;
    }

	@Override
	public Grade findById(Long id) {

		LOG.trace("Start findById(Long id)");

		Grade grade = null;
		try (Connection connection = factory.getConnection()) {
			try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID_SQL)) {
				ps.setLong(1, id);
				try (ResultSet rs = ps.executeQuery()) {
					if (rs.next()) {
						grade = new Grade();
						grade.setId(rs.getLong(1));
						grade.setValue(rs.getLong(2));
						grade.setDescription(rs.getString(3));
					}
				}
			} catch (SQLException e) {
				LOG.throwing(new SQLException("Cannot find this grade in DB", e));
			}
		} catch (SQLException e) {
			LOG.catching(e);
		}
		LOG.trace("End findById(Long id)");

		return grade;
	}
}
