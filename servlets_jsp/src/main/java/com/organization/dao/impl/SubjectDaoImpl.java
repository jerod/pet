package com.organization.dao.impl;

import com.organization.dao.DaoFactory;
import com.organization.dao.api.SubjectDao;
import com.organization.entity.User;
import com.organization.entity.Semester;
import com.organization.entity.Subject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Implements all methods of interface SubjectDao for creating, updating, deleting
 * and finding values in table SUBJECT in DB. Provides access to DB using JDBC API.
 *
 * @author Dmytro Vasylenko
 * @since 1.0
 */
public class SubjectDaoImpl implements SubjectDao {

    private static final String CREATE_SQL = "INSERT INTO subject "
            + "(subject_name, lecturer_id, semester_id) VALUES (?,?,?)";
    private static final String UPDATE_SQL = "UPDATE subject SET "
            + "subject_name = ?, lecturer_id = ?, semester_id = ? "
			+ "WHERE subject_id = ?";
    private static final String REMOVE_SQL =
            "DELETE FROM subject WHERE subject_id = ?";
    private static final String FIND_ALL_SQL = "SELECT * FROM subject";
	private static final String FIND_BY_ID_SQL =
			"SELECT * FROM subject WHERE subject_id = ?";
	private static final Logger LOG = LogManager.getLogger();

	private final DaoFactory factory;

	public SubjectDaoImpl(DaoFactory factory) {
		this.factory = factory;
	}

    @Override
    public void create(Subject subject) {

        LOG.trace("Start create(Subject subject)");

        try (Connection connection = factory.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(CREATE_SQL)) {
                ps.setString(1, subject.getSubjectName());
                ps.setLong(2, subject.getLecturer().getId());
				ps.setLong(3, subject.getSemester().getId());
                ps.executeUpdate();
                connection.commit();

                LOG.trace("create(Subject subject). Transaction executed");
            } catch (SQLException e) {
                connection.rollback();
                LOG.throwing(new SQLException("Transaction failed", e));
            }
        } catch (SQLException e) {
            LOG.catching(e);
        }
        LOG.trace("End create(Subject subject)");
    }

    @Override
    public void update(Subject subject) {

        LOG.trace("Start update(Subject subject)");

        try (Connection connection = factory.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(UPDATE_SQL)) {
                ps.setString(1, subject.getSubjectName());
                ps.setLong(2, subject.getLecturer().getId());
				ps.setLong(3, subject.getSemester().getId());
                ps.setLong(4, subject.getId());
                ps.executeUpdate();
                connection.commit();

                LOG.trace("update(Subject subject). Transaction executed");
            } catch (SQLException e) {
                connection.rollback();
                LOG.throwing(new SQLException("Transaction failed", e));
            }
        } catch (SQLException e) {
            LOG.catching(e);
        }
        LOG.trace("End update(Subject subject)");
    }

    @Override
    public void remove(Subject subject) {

        LOG.trace("Start remove(Subject subject)");

        try (Connection connection = factory.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(REMOVE_SQL)) {
                ps.setLong(1, subject.getId());
                ps.executeUpdate();
                connection.commit();

                LOG.trace("remove(Subject subject). Transaction executed");
            } catch (SQLException e) {
                connection.rollback();
                LOG.throwing(new SQLException("Transaction failed", e));
            }
        } catch (SQLException e) {
            LOG.catching(e);
        }
        LOG.trace("End remove(Subject subject)");
    }

    @Override
    public List<Subject> findAll() {

        LOG.trace("Start findAll()");

        List<Subject> subjects = new ArrayList<>();
        Subject subject;
        try (Connection connection = factory.getConnection()) {
            try (Statement stmt = connection.createStatement()) {
                try (ResultSet rs = stmt.executeQuery(FIND_ALL_SQL)) {
                    while (rs.next()) {
                        subject = new Subject();
						subject.setId(rs.getLong(1));
                        subject.setSubjectName(rs.getString(2));
                        subject.setLecturer(
                        		factory.getUserDao().findById(rs.getLong(3)));
                        subject.setSemester(
                        		factory.getSemesterDao().findById(rs.getLong(4)));
                        subjects.add(subject);
                    }
                }
            } catch (SQLException e) {
                LOG.error("Exception: findAll(). Cannot find subjects");
            }
        } catch (SQLException e) {
           LOG.catching(e);
        }
        LOG.trace("End findAll()");

        return subjects;
    }

	@Override
	public Subject findById(Long id) {

		LOG.trace("Start findById(Long id)");

		Subject subject = null;
		try (Connection connection = factory.getConnection()) {
			try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID_SQL)) {
				ps.setLong(1, id);
				try (ResultSet rs = ps.executeQuery()) {
					if (rs.next()) {
						subject = new Subject();
						subject.setId(rs.getLong(1));
						subject.setSubjectName(rs.getString(2));
						subject.setLecturer(
								factory.getUserDao().findById(rs.getLong(3)));
						subject.setSemester(
								factory.getSemesterDao().findById(rs.getLong(4)));
					}
				}
			} catch (SQLException e) {
				LOG.throwing(new SQLException("Cannot find this subject in DB", e));
			}
		} catch (SQLException e) {
			LOG.catching(e);
		}
		LOG.trace("End findById(Long id)");

		return subject;
	}
}
