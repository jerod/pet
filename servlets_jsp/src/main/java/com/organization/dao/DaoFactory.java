package com.organization.dao;

import com.organization.dao.impl.GradeDaoImpl;
import com.organization.dao.impl.JournalDaoImpl;
import com.organization.dao.impl.RoleDaoImpl;
import com.organization.dao.impl.SemesterDaoImpl;
import com.organization.dao.impl.StatusDaoImpl;
import com.organization.dao.impl.SubjectDaoImpl;
import com.organization.dao.impl.UserDaoImpl;
import org.h2.jdbcx.JdbcConnectionPool;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Class create new Dao.
 */
public class DaoFactory {

    private JdbcConnectionPool pool;

    public DaoFactory() {
    }

    public DaoFactory(JdbcConnectionPool pool) {
        this.pool = pool;
    }

    public Connection getConnection() throws SQLException {
        return pool.getConnection();
    }

    public RoleDaoImpl getRoleDao() {
        return new RoleDaoImpl(this);
    }

    public StatusDaoImpl getStatusDao() {
        return new StatusDaoImpl(this);
    }

    public SemesterDaoImpl getSemesterDao() {
        return new SemesterDaoImpl(this);
    }

    public UserDaoImpl getUserDao() {
        return new UserDaoImpl(this);
    }

	public GradeDaoImpl getGradeDao() {
		return new GradeDaoImpl(this);
	}

	public SubjectDaoImpl getSubjectDao() {
		return new SubjectDaoImpl(this);
	}

	public JournalDaoImpl getJournalDao() {
		return new JournalDaoImpl(this);
	}
}
