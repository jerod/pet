package com.organization.servlet;

import com.organization.dao.DaoFactory;
import com.organization.dao.impl.UserDaoImpl;
import com.organization.entity.User;
import com.organization.util.PropertiesLoader;
import com.organization.util.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.h2.jdbcx.JdbcConnectionPool;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

/**
 * Becomes request from web page. Sets command for user list and invokes
 * parents method for handling this request.
 *
 * @author Dmytro Vasylenko
 * @since 1.0
 */
@WebServlet(name = "users", urlPatterns = "/users")
public class UsersController extends HttpServlet {

	private static final long serialVersionUID = -526883316454168690L;
	private static final Logger LOG = LogManager.getLogger();

	private UserDaoImpl userDao;
	private JdbcConnectionPool pool;

	@Override
	public void init() throws ServletException {

		LOG.trace("Initializing UserController");

		Properties properties = new Properties();
		try {
			properties.load(Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("h2-jdbc.properties"));
			pool = PropertiesLoader.createDb(getServletContext(), properties);
			DaoFactory factory = new DaoFactory(pool);
			userDao = factory.getUserDao();
		} catch (IOException e) {
			LOG.throwing(new IllegalStateException
					("Corrupted property stream connection"));
		}
	}

	@Override
	public void destroy() {
		if (pool != null) {
			pool.dispose();
		}
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		LOG.trace("Start UsersController doGet()");

		User currentUser = (User) req.getAttribute("user");
		List<User> users;

		if (Validator.isAdmin(currentUser)) {
			users = userDao.findAll();
		} else  {
			resp.sendRedirect("index.html");
			return;
		}

		req.setAttribute("users", users);
		req.getRequestDispatcher("WEB-INF/jsp/lists/users.jsp")
				.forward(req, resp);

		LOG.trace("End UsersController doGet()");
	}
}
