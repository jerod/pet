package com.organization.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/**
 * Becomes request from web page. Sets command for view error and invokes
 * parents method for handling this request.
 *
 * @author Dmytro Vasylenko
 * @since 1.0
 */

@WebServlet(name = "error", urlPatterns = "/error")
public class ErrorController extends HttpServlet {

    private static final long serialVersionUID = -4816142400998094927L;
    private static final Logger LOG = LogManager.getLogger();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

        LOG.trace("Start ErrorController doPost()");

		req.setAttribute("redirect_link", req.getHeader("Referer"));
		req.getRequestDispatcher("WEB-INF/jsp/error.jsp").forward(req, resp);

        LOG.trace("End ErrorController doPost()");
    }
}
