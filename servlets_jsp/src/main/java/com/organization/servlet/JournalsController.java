package com.organization.servlet;

import com.organization.dao.DaoFactory;
import com.organization.entity.Journal;
import com.organization.entity.User;
import com.organization.util.PropertiesLoader;
import com.organization.util.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.h2.jdbcx.JdbcConnectionPool;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

/**
 * Becomes request from web page. Sets command for journal list and invokes
 * parents method for handling this request.
 *
 * @author Dmytro Vasylenko
 * @since 1.0
 */
@WebServlet(name = "journals", urlPatterns = "/journals")
public class JournalsController extends HttpServlet {

	private static final long serialVersionUID = -1124419615483317547L;
	private static final Logger LOG = LogManager.getLogger();

	private DaoFactory factory;
	private JdbcConnectionPool pool;

	@Override
	public void init() throws ServletException {

		LOG.trace("Initializing JournalsController");

		Properties properties = new Properties();
		try {
			properties.load(Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("h2-jdbc.properties"));
			pool = PropertiesLoader.createDb(getServletContext(), properties);
			factory = new DaoFactory(pool);
		} catch (IOException e) {
			LOG.throwing(new IllegalStateException
					("Corrupted property stream connection"));
		}
	}

	@Override
	public void destroy() {
		if (pool != null) {
			pool.dispose();
		}
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		LOG.trace("Start JournalsController doGet()");

		User currentUser = (User) req.getAttribute("user");
		List<Journal> journal_entries;

		if (Validator.isAdmin(currentUser)) {
			journal_entries = factory.getJournalDao().findAll();
		} else {
			resp.sendRedirect("index.html");
			return;
		}

		req.setAttribute("journal_entries", journal_entries);
		req.getRequestDispatcher("WEB-INF/jsp/lists/journals.jsp")
				.forward(req, resp);

		LOG.trace("End JournalsController doGet()");
	}
}
