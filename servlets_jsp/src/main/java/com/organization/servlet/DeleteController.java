package com.organization.servlet;

import com.organization.dao.DaoFactory;
import com.organization.entity.User;
import com.organization.util.PropertiesLoader;
import com.organization.util.Validator;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.h2.jdbcx.JdbcConnectionPool;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Properties;

/**
 * Becomes request from web page. Sets command for deleting user and invokes
 * parents method for handling this request.
 *
 * @author Dmytro Vasylenko
 * @since 1.0
 */
@WebServlet("/delete")
public class DeleteController extends HttpServlet {

    private static final long serialVersionUID = -1853371744313434392L;
    private static final Logger LOG = LogManager.getLogger();

    private DaoFactory factory;
    private JdbcConnectionPool pool;

    @Override
    public void init() throws ServletException {

        LOG.trace("Initializing DeleteController");

        Properties properties = new Properties();
        try {
            properties.load(Thread.currentThread().getContextClassLoader()
                    .getResourceAsStream("h2-jdbc.properties"));
            pool = PropertiesLoader.createDb(getServletContext(), properties);
            factory = new DaoFactory(pool);
        } catch (IOException e) {
            LOG.throwing(new IllegalStateException
                    ("Corrupted property stream connection"));
        }
    }

    @Override
    public void destroy() {
        if (pool != null) {
            pool.dispose();
        }
    }

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		LOG.trace("Start DeleteController doGet()");

		String userId = req.getParameter("userId");
		User userToDelete = null;

		if (StringUtils.isNumeric(userId)) {
			userToDelete = factory.getUserDao().findById(Long.parseLong(userId));
		} else {
			LOG.throwing(new IllegalStateException(
					"There is no such user to delete"));
		}
		req.setAttribute("userToDelete", userToDelete);
		req.getRequestDispatcher("WEB-INF/jsp/delete.jsp").forward(req, resp);

		LOG.trace("End DeleteController doGet()");
	}

	@Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        LOG.trace("Start DeleteController doPost()");

		String id = req.getParameter("person_id");
		User userToDelete = null;

		if (StringUtils.isNumeric(id)) {
			userToDelete = factory.getUserDao().findById(Long.parseLong(id));
		}

		if (userToDelete != null && Boolean.valueOf(req.getParameter("decision"))) {
			factory.getUserDao().remove(userToDelete);
		}
		resp.sendRedirect("users");

		LOG.trace("End DeleteController doPost()");
    }
}
