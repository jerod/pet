package com.organization.servlet;

import com.organization.dao.DaoFactory;
import com.organization.dao.api.UserDao;
import com.organization.dao.impl.UserDaoImpl;
import com.organization.entity.Role;
import com.organization.entity.User;
import com.organization.util.PropertiesLoader;
import com.organization.util.RequestMap;
import com.organization.util.Validator;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.h2.jdbcx.JdbcConnectionPool;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

/**
 * Becomes request from web page. Sets command for view user and invokes
 * parents method for handling this request.
 *
 * @author Dmytro Vasylenko
 * @since 1.0
 */
@WebServlet(name = "user", urlPatterns = "/user")
public class UserController extends HttpServlet {

    private static final long serialVersionUID = 2785439679236154788L;
    private static final Logger LOG = LogManager.getLogger();

	private DaoFactory factory;
    private JdbcConnectionPool pool;

    @Override
    public void init() throws ServletException {

        LOG.trace("Initializing UserController");

        Properties properties = new Properties();
        try {
            properties.load(Thread.currentThread().getContextClassLoader()
                    .getResourceAsStream("h2-jdbc.properties"));
            pool = PropertiesLoader.createDb(getServletContext(), properties);
			factory = new DaoFactory(pool);
        } catch (IOException e) {
            LOG.throwing(new IllegalStateException
                    ("Corrupted property stream connection"));
        }
    }

    @Override
    public void destroy() {
        if (pool != null) {
            pool.dispose();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        LOG.trace("Start UserController doGet()");

		User currentUser = (User) req.getAttribute("user");
		List<Role> roles;
		if (Validator.isAdmin(currentUser)) {
			roles = factory.getRoleDao().findAll();
		} else {
			resp.sendRedirect("index.html");
			return;
		}

		req.setAttribute("roles", roles);
		req.setAttribute("semesters", factory.getSemesterDao().findAll());
		req.setAttribute("statuses", factory.getStatusDao().findAll());

		String userId = req.getParameter("userId");
		User userToEdit = null;

		if (StringUtils.isNumeric(userId)) {
			userToEdit = factory.getUserDao().findById(Long.parseLong(userId));
		}
		req.setAttribute("userToEdit", userToEdit);
		req.getRequestDispatcher("WEB-INF/jsp/edits/user_edit.jsp")
				.forward(req, resp);

        LOG.trace("End UserController doGet()");
    }

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		LOG.trace("Start UserController doPost()");

		User currentUser = (User) req.getAttribute("user");
		if (!Validator.isAdmin(currentUser)) {
			resp.sendRedirect("index.html");
		}
		User person = RequestMap.mapUser(factory, req);
		UserDaoImpl personDao = factory.getUserDao();

		if (person.getId() == null) {
			personDao.create(person);
		} else {
			personDao.update(person);
		}
		resp.sendRedirect("users");

		LOG.trace("End UserController doPost()");
	}
}
