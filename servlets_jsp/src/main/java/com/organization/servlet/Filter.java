package com.organization.servlet;

import com.organization.dao.DaoFactory;
import com.organization.entity.User;
import com.organization.util.Authenticator;
import com.organization.util.PropertiesLoader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.h2.jdbcx.JdbcConnectionPool;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Properties;

/**
 * Class - filter for roles;
 *
 * @author Dmytro Vasylenko
 * @since 1.0
 */
@WebFilter(filterName = "auth", servletNames = {"user", "users",
		"profile", "journal", "journals", "semester", "semesters",
		"subject", "subjects", "delete"})
public class Filter implements javax.servlet.Filter {

    private static final Logger LOG = LogManager.getLogger();

    private JdbcConnectionPool pool;
    private Authenticator auth;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

        LOG.trace("Initializing Filter");

        Properties properties = new Properties();
        try {
            properties.load(Thread.currentThread().getContextClassLoader()
                    .getResourceAsStream("h2-jdbc.properties"));
            pool = PropertiesLoader
                    .createDb(filterConfig.getServletContext(), properties);
            auth = new Authenticator(new DaoFactory(pool));
        } catch (IOException e) {
            LOG.throwing(new IllegalStateException(
                    "Corrupted property stream connection"));
        }
    }

    @Override
    public void doFilter(
            ServletRequest servletRequest, ServletResponse servletResponse,
            FilterChain filterChain) throws IOException, ServletException {

        LOG.trace("Start doFilter()");

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        User user = auth.getSessionAuth(request.getSession());
        request.setAttribute("user", user);
        if (user == null) {
            response.sendRedirect("index.html");
        }
		request.setAttribute("user", user);
        filterChain.doFilter(servletRequest, servletResponse);

        LOG.trace("End doFilter()");
    }

    @Override
    public void destroy() {
        if (pool != null) {
            pool.dispose();
        }
    }
}
