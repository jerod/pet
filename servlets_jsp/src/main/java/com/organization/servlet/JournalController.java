package com.organization.servlet;

import com.organization.dao.DaoFactory;
import com.organization.entity.Journal;
import com.organization.entity.User;
import com.organization.util.PropertiesLoader;
import com.organization.util.RequestMap;
import com.organization.util.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.h2.jdbcx.JdbcConnectionPool;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Properties;

/**
 * Becomes request from web page. Sets command for view journal and invokes
 * parents method for handling this request.
 *
 * @author Dmytro Vasylenko
 * @since 1.0
 */
@WebServlet(name = "journal", urlPatterns = "/journal")
public class JournalController extends HttpServlet {

	private static final long serialVersionUID = 4138884778556376391L;
	private static final Logger LOG = LogManager.getLogger();

	private DaoFactory factory;
	private JdbcConnectionPool pool;

	@Override
	public void init() throws ServletException {

		LOG.trace("Initializing JournalsController");

		Properties properties = new Properties();
		try {
			properties.load(Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("h2-jdbc.properties"));
			pool = PropertiesLoader.createDb(getServletContext(), properties);
			factory = new DaoFactory(pool);
		} catch (IOException e) {
			LOG.throwing(new IllegalStateException
					("Corrupted property stream connection"));
		}
	}

	@Override
	public void destroy() {
		if (pool != null) {
			pool.dispose();
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		LOG.trace("Start JournalController doPost()");

		User currentUser = (User) req.getAttribute("user");

		if (!Validator.isAdmin(currentUser)) {
			resp.sendRedirect("index.html");
			return;
		}

		Journal journal = RequestMap.mapJournal(factory, req);
		factory.getJournalDao().create(journal);
		resp.sendRedirect("journals");

		LOG.trace("End JournalController doPost()");
	}
}
