package com.organization.servlet;

import java.io.IOException;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.organization.dao.DaoFactory;
import com.organization.entity.User;
import com.organization.util.Authenticator;
import com.organization.util.PropertiesLoader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.h2.jdbcx.JdbcConnectionPool;

/**
 * Becomes request from web page. Sets command for checking user and logging to
 * a system. Invokes parents method for handling this request.
 *
 * @author Dmytro Vasylenko
 * @since 1.0
 */
@WebServlet(name = "login", urlPatterns = "/login")
public class LoginController extends HttpServlet {

    private static final long serialVersionUID = -4532384812794041391L;
    private static final Logger LOG = LogManager.getLogger();

    private JdbcConnectionPool pool;
    private Authenticator auth;

    @Override
    public void init() throws ServletException {

        LOG.trace("Initializing LoginController");

        Properties properties = new Properties();
        try {
            properties.load(Thread.currentThread().getContextClassLoader()
                    .getResourceAsStream("h2-jdbc.properties"));
            pool = PropertiesLoader.createDb(getServletContext(), properties);
            auth = new Authenticator(new DaoFactory(pool));
        } catch (IOException e) {
            LOG.throwing(new IllegalStateException
                    ("Corrupted property stream connection"));
        }
    }

    @Override
    public void destroy() {
        if (pool != null) {
            pool.dispose();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        LOG.trace("Start LoginController doPost()");

        String login = req.getParameter("login");
        String password = req.getParameter("password");

        User user = auth.authenticate(login, password);


        if (user == null) {
			LOG.error("Exception: Wrong credentials");
			resp.sendRedirect("error?errMsg=Wrong credentials");
			return;
        } else {
			req.getSession(true).setAttribute("login", user.getLogin());
			req.getSession(true).setAttribute("password", user.getPassword());
			resp.sendRedirect("profile");
        }

        LOG.trace("End LoginController doPost()");
    }
}
