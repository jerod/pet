package com.organization.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Becomes request from web page. Sets command for profile of user and invokes
 * parents method for handling this request.
 *
 * @author Dmytro Vasylenko
 * @since 1.0
 */
@WebServlet(name = "profile", urlPatterns = "/profile")
public class ProfileController extends HttpServlet {

	private static final long serialVersionUID = -3685601186014594252L;
	private static final Logger LOG = LogManager.getLogger();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		LOG.trace("Start ProfileController doGet()");

		req.getRequestDispatcher("WEB-INF/jsp/profile.jsp")
				.forward(req, resp);

		LOG.trace("End ProfileController doGet()");
	}
}
