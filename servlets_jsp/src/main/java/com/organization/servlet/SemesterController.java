package com.organization.servlet;

import com.organization.dao.DaoFactory;
import com.organization.dao.impl.SemesterDaoImpl;
import com.organization.entity.Semester;
import com.organization.util.PropertiesLoader;
import com.organization.util.RequestMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.h2.jdbcx.JdbcConnectionPool;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Properties;

/**
 * Becomes request from web page. Sets command for view semester and invokes
 * parents method for handling this request.
 *
 * @author Dmytro Vasylenko
 * @since 1.0
 */
@WebServlet(name = "semester", urlPatterns = "/semester")
public class SemesterController extends HttpServlet {

	private static final long serialVersionUID = 4748581700915742293L;
	private static final Logger LOG = LogManager.getLogger();

	private DaoFactory factory;
	private JdbcConnectionPool pool;

	@Override
	public void init() throws ServletException {

		LOG.trace("Initializing JournalsController");

		Properties properties = new Properties();
		try {
			properties.load(Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("h2-jdbc.properties"));
			pool = PropertiesLoader.createDb(getServletContext(), properties);
			factory = new DaoFactory(pool);
		} catch (IOException e) {
			LOG.throwing(new IllegalStateException
					("Corrupted property stream connection"));
		}
	}

	@Override
	public void destroy() {
		if (pool != null) {
			pool.dispose();
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		LOG.trace("Start SemesterController doPost()");

		Semester semester = RequestMap.mapSemester(req);
		SemesterDaoImpl semesterDao = factory.getSemesterDao();

		if (semester.getId() == null) {
			semesterDao.create(semester);
		} else {
			semesterDao.update(semester);
		}
		resp.sendRedirect("semesters");

		LOG.trace("End SemesterController doPost()");
	}
}
