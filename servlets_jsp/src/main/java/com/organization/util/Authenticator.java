package com.organization.util;

import com.organization.dao.DaoFactory;
import com.organization.entity.User;

import javax.servlet.http.HttpSession;

public class Authenticator {

    private DaoFactory factory;

    public Authenticator(DaoFactory factory) {
        this.factory = factory;
    }

    Validator validator = new Validator();

    public User authenticate(String login, String password) {
        if (validator.isLoginCorrect(login) && validator.isPasswordCorrect(password)) {
            User user = factory.getUserDao().findByLogin(login);
            if (user != null && user.getPassword().equals(password)) {
                return user;
            }
        }
        return null;
    }

    public User getSessionAuth(HttpSession session) {

        String login;
        String password;

        if (session == null
                || (login = (String) session.getAttribute("login")) == null
                || (password = (String) session.getAttribute("password")) == null) {

            return null;
        } else {
            return authenticate(login, password);
        }
    }
}
