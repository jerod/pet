package com.organization.util;

import com.organization.entity.User;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class contains methods for validation parameters.
 *
 * @author Dmytro Vasylenko
 * @since 1.0
 */
public class Validator {

    private static final Pattern LOGIN_PATTERN = Pattern
            .compile("^[a-zA-Z0-9_-]{3,16}$");
    private static final Pattern PASSWORD_PATTERN = Pattern
            .compile("^[a-zA-Z0-9_-]{3,16}$");

    private Matcher matcher;

    /**
     * Method checking correct login.
     * @param login     login
     * @return          <tt>true</tt> if login correct.
     */
    public boolean isLoginCorrect(String login) {
        matcher = LOGIN_PATTERN.matcher(login);
        return matcher.matches();
    }

    /**
     * Method checking correct password.
     * @param password  password
     * @return          <tt>true</tt> if login correct.
     */
    public boolean isPasswordCorrect(String password) {
        matcher = PASSWORD_PATTERN.matcher(password);
        return matcher.matches();
    }

	public static boolean isAdmin(User user) {
		return user != null && user.getRole().getName().equals("admin");
	}

}