package com.organization.util;

import java.io.File;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.h2.jdbcx.JdbcConnectionPool;

import javax.servlet.ServletContext;

/**
 * Provides method for loading properties as resource from src directory.
 *
 * @author Dmytro Vasylenko
 * @since 1.0
 */
public class PropertiesLoader {

    private static final Logger LOG = LogManager.getLogger();

    public PropertiesLoader() {
    }

    /**
     * Load properties as resource from file.
     */
    public static JdbcConnectionPool createDb
    (ServletContext context, Properties prop) {

        LOG.trace("Start createDb()");

        String dbName = prop.getProperty("database");
        String dbPath = context.getRealPath("/");
        StringBuilder url = new StringBuilder();

        url.append("jdbc:h2:").append(dbPath).append("WEB-INF")
                .append(File.separator).append("classes")
                .append(File.separator).append("db")
                .append(File.separator).append(dbName);
        return JdbcConnectionPool.create(
                url.toString(),
                prop.getProperty("login"),
                prop.getProperty("password"));
    }
}
