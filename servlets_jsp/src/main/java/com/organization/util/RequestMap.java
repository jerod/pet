package com.organization.util;

import com.organization.dao.DaoFactory;
import com.organization.entity.Journal;
import com.organization.entity.Semester;
import com.organization.entity.Subject;
import com.organization.entity.User;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;

/**
 * Class contains methods for request content parameters.
 *
 * @author Dmytro Vasylenko
 * @since 1.0
 */
public class RequestMap {

	private static final Logger LOG = LogManager.getLogger();

	static Validator validator = new Validator();

	public RequestMap() {
	}

	public static User mapUser(DaoFactory factory, HttpServletRequest req)
			throws IllegalStateException {

		User person = new User();
		String id = req.getParameter("person_id");
		Long role_id = null;
		Long status_id = null;
		Long current_semester_id = null;

		if (StringUtils.isNumeric(id)) {
			person.setId(Long.valueOf(id));
		}

		person.setFirstName(req.getParameter("first_name"));
		person.setMiddleName(req.getParameter("middle_name"));
		person.setLastName(req.getParameter("last_name"));
		person.setLogin(req.getParameter("login"));
		person.setPassword(req.getParameter("password"));
		person.setPhoneNumber(req.getParameter("phone_number"));
		String birth_date = req.getParameter("birth_date");
		String enrollment_date = req.getParameter("enrollment_date");

		if (StringUtils.isAnyBlank(
				person.getLogin(),
				person.getPassword(),
				person.getFirstName(),
				person.getLastName())) {
			LOG.throwing(new IllegalStateException("Not all fields are filled"));
		}

		if (!validator.isLoginCorrect(person.getLogin())
				|| !validator.isPasswordCorrect(person.getPassword())) {
			LOG.throwing(
					new IllegalStateException("Wrong login/password format"));
		}

		try {
			if (!StringUtils.isBlank(birth_date)) {
				person.setBirthday(Date.valueOf(birth_date));
			} else {
				person.setBirthday(null);
			}
			if (!StringUtils.isBlank(enrollment_date)) {
				person.setEnrollmentDate(Date.valueOf(enrollment_date));
			} else {
				person.setEnrollmentDate(null);
			}
		} catch (IllegalArgumentException e) {
			LOG.throwing(
					new IllegalStateException("Corrupted date format"));
		}

		try {
			role_id = NumberUtils
					.createLong(req.getParameter("role_id"));
			status_id = NumberUtils
					.createLong(req.getParameter("status_id"));
			current_semester_id = NumberUtils
					.createLong(req.getParameter("current_semester_id"));
		} catch (NumberFormatException e) {
			LOG.throwing(
					new IllegalStateException("Obtained id are not numbers"));
		}

		person.setRole(factory.getRoleDao().findById(role_id));
		person.setStatus(factory.getStatusDao().findById(status_id));
		person.setCurrentSemester(
				factory.getSemesterDao().findById(current_semester_id));

		if (!ObjectUtils.allNotNull(
				person.getRole(),
				person.getCurrentSemester(),
				person.getCurrentSemester())) {
			LOG.throwing(new IllegalStateException(
					"Corrupted or obsolete select data obtained"));
		}
		return person;
	}

	public static Semester mapSemester(HttpServletRequest req) {

		Semester semester = new Semester();
		String id = req.getParameter("semester_id");
		String startDate = req.getParameter("start_date");
		String endDate = req.getParameter("end_date");

		if (StringUtils.isNumeric(id)) {
			semester.setId(Long.valueOf(id));
		}
		try {
			semester.setSemesterNumber(NumberUtils.createLong(
					req.getParameter("semester_number")));
		} catch (NumberFormatException e) {
			LOG.throwing(new IllegalStateException(
					"Corrupted semester number obtained"));
		}

		if (StringUtils.isAnyBlank(startDate, endDate)) {
			LOG.throwing(new IllegalStateException("Not all fields are filled"));
		}
		try {
			semester.setStartDate(Date.valueOf(startDate));
			semester.setEndDate(Date.valueOf(endDate));
		} catch (IllegalArgumentException e) {
			LOG.throwing(new IllegalStateException("Corrupted date obtained"));
		}
		return semester;
	}

	public static Subject mapSubject(DaoFactory factory, HttpServletRequest req) {

		Subject subject = new Subject();
		String id = req.getParameter("subject_id");
		Long lecturer_id = null;
		Long semester_id = null;

		if (StringUtils.isNumeric(id)) {
			subject.setId(Long.valueOf(id));
		}

		subject.setSubjectName(req.getParameter("subject_name"));

		if (StringUtils.isBlank(subject.getSubjectName())) {
			LOG.throwing(new IllegalStateException(
					"Field subject name was not filled properly"));
		}

		try {
			lecturer_id = NumberUtils
					.createLong(req.getParameter("lecturer_id"));
			semester_id = NumberUtils
					.createLong(req.getParameter("semester_id"));
		} catch (NumberFormatException e) {
			LOG.throwing(new IllegalStateException(
					"Obtained id are not numbers"));
		}

		subject.setLecturer(factory.getUserDao().findById(lecturer_id));
		subject.setSemester(factory.getSemesterDao().findById(semester_id));

		if (!ObjectUtils.allNotNull(subject.getLecturer(), subject.getSemester())) {
			LOG.throwing(new IllegalStateException(
					"Corrupted or obsolete select data obtained"));
		}
		return subject;
	}

	public static Journal mapJournal(DaoFactory factory, HttpServletRequest req) {

		Journal journal = new Journal();
		String gradeDate = req.getParameter("grade_date");
		Long student_id = null;
		Long subject_id = null;
		Long grade_id = null;

		try {
			student_id = NumberUtils
					.createLong(req.getParameter("student_id"));
			subject_id = NumberUtils
					.createLong(req.getParameter("subject_id"));
			grade_id = NumberUtils
					.createLong(req.getParameter("grade_id"));
		} catch (NumberFormatException e) {
			LOG.throwing(new IllegalStateException(
					"Obtained id are not numbers"));
		}

		journal.setStudent(factory.getUserDao().findById(student_id));
		journal.setSubject(factory.getSubjectDao().findById(subject_id));
		journal.setGrade(factory.getGradeDao().findById(grade_id));

		if (StringUtils.isBlank(gradeDate)) {
			LOG.throwing(new IllegalStateException(
					"Grade date is a mandatory field"));
		}
		try {
			journal.setGradeDate(Date.valueOf(gradeDate));
		} catch (IllegalArgumentException e) {
			LOG.throwing(new IllegalArgumentException(
					"Corrupted data field"));
		}
		return journal;
	}
}
