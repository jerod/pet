<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html">
    <title>Teacher's page</title>
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet">
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet">
</head>
<body>
<div class="success">
    Dear <strong>${user}</strong>, Welcome to Teacher's Page.
    <br/>
    <a href="<c:url value="/logout" />">Logout</a>
</div>
</body>
</html>