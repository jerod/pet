<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html">
    <title>All User's page</title>
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet">
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet">
</head>
<body>
<div class="table-container">
<h2>List of Users</h2>
<table>
    <tr>
        <td>Name</td>
        <td>Surname</td>
        <td>Login</td>
        <td>Email</td>
        <td>Password</td>
        <td>State</td>
        <td>Profile</td>
        <td>Modify</td>
        <td>Remove</td>
    </tr>
    <c:forEach items="${users}" var="user">
        <tr>
            <td>${user.firstName}</td>
            <td>${user.lastName}</td>
            <td>${user.login}</td>
            <td>${user.email}</td>
            <td>${user.password}</td>
            <td>${user.state}</td>
            <td>${user.userProfiles}</td>
            <td><a href="<c:url value='/edit-user-${user.login}' />">update</a></td>
            <td><a href="<c:url value='/delete-${user.login}-user' />">delete</a></td>
        </tr>
    </c:forEach>
</table>
</div>

</body>
</html>