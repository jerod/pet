<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html">
    <title>Welcome page</title>
    <link href="<c:url value='/static/css/bootstrap.css' />"  rel="stylesheet">
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet">
</head>
<body>
<div class="success">
    Greeting : ${greeting}
    This is a welcome page.</br>
    <a href="<c:url value="/login" />">Login page</a></br>
    <a href="<c:url value="/teacher" />">Teacher page</a></br>
    <a href="<c:url value="/admin" />">Admin page</a>

</div>
</body>
</html>