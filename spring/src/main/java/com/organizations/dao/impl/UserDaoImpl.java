package com.organizations.dao.impl;

import com.organizations.dao.AbstractDao;
import com.organizations.dao.api.UserDao;
import com.organizations.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Long, User> implements UserDao {

	private static final Logger LOG = LogManager.getLogger();

	@Override
	public User findById(Long id) {

		LOG.trace("Start findById()");

		return getByKey(id);
	}

	@Override
	public User findByLogin(String login) {

		LOG.trace("Start findByLogin()");

		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("login", login));

		return (User) criteria.uniqueResult();
	}

	@Override
	public void save(User user) {

		LOG.trace("Start save()");

		persist(user);

		LOG.trace("End save()");
	}

	@Override
	public void delete(String login) {

		LOG.trace("Start delete()");

		Query query = getSession().createSQLQuery("delete from App_User where SSO_ID = :login");
		query.setString("login", login);
		query.executeUpdate();

		LOG.trace("End delete()");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> findAllUsers() {

		LOG.trace("Start findAllUsers()");

		Criteria criteria = createEntityCriteria();

		return (List<User>) criteria.list();
	}
}
