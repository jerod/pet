package com.organizations.dao.impl;

import com.organizations.dao.AbstractDao;
import com.organizations.dao.api.UserProfileDao;
import com.organizations.model.UserProfile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("userProfileDao")
public class UserProfileDaoImpl extends AbstractDao<Long, UserProfile> implements UserProfileDao {

	private static final Logger LOG = LogManager.getLogger();

	@SuppressWarnings("unchecked")
	@Override
	public List<UserProfile> findAll() {

		LOG.trace("Start findAll()");

		Criteria criteria = createEntityCriteria();
		criteria.addOrder(Order.asc("type"));

		return (List<UserProfile>)criteria.list();
	}

	@Override
	public UserProfile findByType(String type) {

		LOG.trace("Start findByType()");

		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("type", type));

		return (UserProfile)criteria.uniqueResult();
	}

	@Override
	public UserProfile findById(Long id) {

		LOG.trace("Start findById()");

		return getByKey(id);
	}
}
