package com.organizations.dao.api;

import com.organizations.model.User;

import java.util.List;

public interface UserDao {

	User findById(Long id);

	User findByLogin(String login);

	void save(User user);

	void delete(String login);

	List<User> findAllUsers();


}
