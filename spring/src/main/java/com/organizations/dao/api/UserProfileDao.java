package com.organizations.dao.api;

import com.organizations.model.UserProfile;

import java.util.List;

public interface UserProfileDao {

	List<UserProfile> findAll();

	UserProfile findByType(String type);

	UserProfile findById(Long id);
}
