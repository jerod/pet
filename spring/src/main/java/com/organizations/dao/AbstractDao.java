package com.organizations.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;

public abstract class AbstractDao<PK extends Serializable, T> {

	private static final Logger LOG = LogManager.getLogger();
	private final Class<T> persistentClass;

	@SuppressWarnings("unchecked")
	public AbstractDao(){
		this.persistentClass =(Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[1];
	}

	@Autowired
	private SessionFactory sessionFactory;

	protected Session getSession(){
		return sessionFactory.getCurrentSession();
	}

	@SuppressWarnings("unchecked")
	public T getByKey(PK key) {

		LOG.trace("Start getByKey()");

		return (T) getSession().get(persistentClass, key);
	}

	public void persist(T entity) {

		LOG.trace("Start persist()");

		getSession().persist(entity);
	}

	public void delete(T entity) {

		LOG.trace("Start delete()");

		getSession().delete(entity);
	}

	protected Criteria createEntityCriteria(){

		LOG.trace("Start createEntityCriteria()");

		return getSession().createCriteria(persistentClass);
	}
}
