package com.organizations.model;

public enum UserProfileType {

	USER("USER"),
	TEACHER("TEACHER"),
	ADMIN("ADMIN");

	String userProfileType;

	private UserProfileType(String userProfileType) {
		this.userProfileType = userProfileType;
	}

	public String getUserProfileType() {
		return userProfileType;
	}
}
