package com.organizations.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class SpringMvcInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	private static final Logger LOG = LogManager.getLogger();

	@Override
	protected Class<?>[] getRootConfigClasses() {

		LOG.trace("Init getRootConfigClasses()");

		return new Class[] { AppConfiguration.class };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {

		LOG.trace("Init getServletConfigClasses()");

		return null;
	}

	@Override
	protected String[] getServletMappings() {

		LOG.trace("Init getServletMappings()");

		return new String[] { "/" };
	}
}
