package com.organizations.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan({ "com.organizations.configuration" })
@PropertySource(value = { "classpath:application.properties" })
public class HibernateConfiguration {

	private static final Logger LOG = LogManager.getLogger();

	@Autowired
	private Environment environment;

	@Bean
	public LocalSessionFactoryBean sessionFactory() {

		LOG.trace("Start sessionFactory()");

		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();

		sessionFactory.setDataSource(dataSource());
		sessionFactory.setPackagesToScan(new String[] { "com.organizations.model" });
		sessionFactory.setHibernateProperties(hibernateProperties());

		return sessionFactory;
	}

	@Bean
	public DataSource dataSource() {

		LOG.trace("Init @Bean dataSource()");

		DriverManagerDataSource dataSource = new DriverManagerDataSource();

		dataSource.setDriverClassName(
				environment.getRequiredProperty("jdbc.driverClassName"));
		dataSource.setUrl(
				environment.getRequiredProperty("jdbc.url"));
		dataSource.setUsername(
				environment.getRequiredProperty("jdbc.username"));
		dataSource.setPassword(
				environment.getRequiredProperty("jdbc.password"));

		return dataSource;
	}

	private Properties hibernateProperties() {

		LOG.trace("Start hibernateProperties()");

		Properties properties = new Properties();

		properties.put(
				"hibernate.dialect",
				environment.getRequiredProperty("hibernate.dialect"));
		properties.put(
				"hibernate.show_sql",
				environment.getRequiredProperty("hibernate.show_sql"));
		properties.put(
				"hibernate.format_sql",
				environment.getRequiredProperty("hibernate.format_sql"));

		return properties;
	}

	@Bean
	@Autowired
	public HibernateTransactionManager transactionManager(SessionFactory s) {

		LOG.trace("Init @Bean transactionManager()");

		HibernateTransactionManager txManager = new HibernateTransactionManager();

		txManager.setSessionFactory(s);

		return txManager;
	}
}
