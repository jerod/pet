package com.organizations.configuration;

import com.organizations.model.UserProfile;
import com.organizations.service.api.UserProfileService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class RoleToUserProfileConverter implements Converter<Object, UserProfile> {

	private static final Logger LOG = LogManager.getLogger();

	@Autowired
	UserProfileService userProfileService;

	@Override
	public UserProfile convert(Object o) {

		LOG.trace("Start convert()");

		Long id = Long.parseLong((String)o);
		UserProfile profile = userProfileService.findById(id);
		System.out.println("Profile : " + profile);

		LOG.trace("End convert()");
		return profile;
	}
}
