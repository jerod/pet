package com.organizations.service.api;

import com.organizations.model.User;

import java.util.List;

public interface UserService {

	User findById(Long id);

	User findByLogin(String login);

	void save(User user);

	void update(User user);

	void delete(String login);

	List<User> findAllUsers();

	boolean isUserLoginUnique(Long id, String login);
}
