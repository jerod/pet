package com.organizations.service.api;

import com.organizations.model.UserProfile;

import java.util.List;

public interface UserProfileService {

	List<UserProfile> findAll();

	UserProfile findByType(String type);

	UserProfile findById(Long id);
}
