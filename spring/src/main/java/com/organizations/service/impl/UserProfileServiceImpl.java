package com.organizations.service.impl;

import com.organizations.dao.api.UserProfileDao;
import com.organizations.model.UserProfile;
import com.organizations.service.api.UserProfileService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("userProfileService")
@Transactional
public class UserProfileServiceImpl implements UserProfileService {

	private static final Logger LOG = LogManager.getLogger();

	@Autowired
	UserProfileDao dao;

	@Override
	public List<UserProfile> findAll() {

		LOG.trace("Start findAll()");

		return dao.findAll();
	}

	@Override
	public UserProfile findByType(String type) {

		LOG.trace("Start findByType()");

		return dao.findByType(type);
	}

	@Override
	public UserProfile findById(Long id) {

		LOG.trace("Start findById()");

		return dao.findById(id);
	}
}
