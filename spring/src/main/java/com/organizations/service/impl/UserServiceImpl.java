package com.organizations.service.impl;

import com.organizations.dao.api.UserDao;
import com.organizations.model.User;
import com.organizations.service.api.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

	private static final Logger LOG = LogManager.getLogger();

	@Autowired
	private UserDao dao;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public User findById(Long id) {

		LOG.trace("Start findById()");

		return dao.findById(id);
	}

	@Override
	public User findByLogin(String login) {

		LOG.trace("Start findByLogin()");

		return dao.findByLogin(login);
	}

	@Override
	public void save(User user) {

		LOG.trace("Start save()");

		user.setPassword(passwordEncoder.encode(user.getPassword()));
		dao.save(user);

		LOG.trace("End save()");
	}

	@Override
	public void update(User user) {

		LOG.trace("Start update()");

		User entity = dao.findById(user.getId());
		if (entity != null) {
			entity.setFirstName(user.getFirstName());
			entity.setLastName(user.getLastName());
			entity.setEmail(user.getEmail());
			entity.setLogin(user.getLogin());
			entity.setPassword(user.getPassword());
			entity.setState(user.getState());
			entity.setUserProfiles(user.getUserProfiles());
		}

		LOG.trace("End update()");
	}

	@Override
	public void delete(String login) {

		LOG.trace("Start delete()");

		dao.delete(login);

		LOG.trace("End delete()");
	}

	@Override
	public List<User> findAllUsers() {

		LOG.trace("Start findAllUsers()");

		return dao.findAllUsers();
	}

	@Override
	public boolean isUserLoginUnique(Long id, String login) {

		LOG.trace("Start isUserLoginUnique()");

		User user = findByLogin(login);
		return (user == null || ((id != null) && (user.getId() == id)));
	}
}
