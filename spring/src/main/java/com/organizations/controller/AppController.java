package com.organizations.controller;

import com.organizations.model.User;
import com.organizations.model.UserProfile;
import com.organizations.service.api.UserProfileService;
import com.organizations.service.api.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.Locale;

@Controller
@RequestMapping("/")
@SessionAttributes("roles")
public class AppController {

	private static final Logger LOG = LogManager.getLogger();

	@Autowired
	UserProfileService userProfileService;

	@Autowired
	UserService service;

	@Autowired
	MessageSource messageSource;

	@RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
	public String homePage(ModelMap model) {

		LOG.trace("Init homePage(), RequestMethod.GET");

		model.addAttribute("greeting",
				"Hi, Welcome to mysite");
		return "welcome";
	}

	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public String adminPage(ModelMap model) {

		LOG.trace("Init adminPage(), RequestMethod.GET");

		model.addAttribute("user", getPrincipal());
		return "admin";
	}

	@RequestMapping(value = "/teacher", method = RequestMethod.GET)
	public String teacherPage(ModelMap model) {

		LOG.trace("Init teacherPage(), RequestMethod.GET");

		model.addAttribute("user", getPrincipal());
		return "teacher";
	}

	@RequestMapping(value = "/Access_Denied", method = RequestMethod.GET)
	public String accessDeniedPage(ModelMap model) {

		LOG.trace("Init accessDeniedPage(), RequestMethod.GET");

		model.addAttribute("user", getPrincipal());
		return "accessDenied";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginPage() {

		LOG.trace("Init loginPage(), RequestMethod.GET");

		return "login";
	}

	@RequestMapping(value="/logout", method = RequestMethod.GET)
	public String logoutPage (HttpServletRequest request,
							  HttpServletResponse response) {

		LOG.trace("Init logoutPage(), RequestMethod.GET");

		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "redirect:/login?logout";
	}

	@RequestMapping(value = { "/list" }, method = RequestMethod.GET)
	public String listUsers(ModelMap model) {

		LOG.trace("Init listUsers(), RequestMethod.GET");

		List<User> users = service.findAllUsers();
		model.addAttribute("users", users);
		return "allusers";
	}

	@RequestMapping(value = { "/newuser" }, method = RequestMethod.GET)
	public String newUser(ModelMap model) {

		LOG.trace("Init newUser(), RequestMethod.GET");

		User user = new User();
		model.addAttribute("user", user);
		model.addAttribute("edit", false);
		return "registration";
	}

	@RequestMapping(value = { "/newuser" }, method = RequestMethod.POST)
	public String saveUser(@Valid User user, BindingResult result,
						   ModelMap model) {

		LOG.trace("Init saveUser(), RequestMethod.GET");

		if (result.hasErrors()) {
			return "registration";
		}

		if (!service.isUserLoginUnique(user.getId(), user.getLogin())) {
			FieldError loginError = new FieldError(
					"user",
					"login",
					messageSource.getMessage(
							"non.unique.login",
							new String[]{user.getLogin()},
							Locale.getDefault()
					)
			);
			result.addError(loginError);

			return "registration";
		}

		service.save(user);

		model.addAttribute("success",
				"User " + user.getFirstName() + " "
						+ user.getLastName() + " registered successfully");

		return "registrationsuccess";
	}

	@RequestMapping(value = { "/edit-user-{login}" }, method = RequestMethod.GET)
	public String editUser(@PathVariable String login, ModelMap model) {

		LOG.trace("Init editUser(), RequestMethod.GET");

		User user = service.findByLogin(login);
		model.addAttribute("user", user);
		model.addAttribute("edit", true);
		return "registration";
	}

	@RequestMapping(value = { "/edit-user-{login}" }, method = RequestMethod.POST)
	public String updateUser(@Valid User user, BindingResult result,
							 ModelMap model, @PathVariable String login) {

		LOG.trace("Init updateUser(), RequestMethod.POST");

		if (result.hasErrors()) {
			return "registration";
		}

		service.update(user);

		model.addAttribute("success",
				"User " + user.getFirstName() + " "
						+ user.getLastName() + " updated successfully");

		return "registrationsuccess";
	}

	@RequestMapping(value = { "/delete-{login}-user" }, method = RequestMethod.GET)
	public String deleteUser(@PathVariable String login) {

		LOG.trace("Init deleteUser(), RequestMethod.GET");

		service.delete(login);
		return "redirect:/list";
	}

	private String getPrincipal() {

		LOG.trace("Start getPrincipal()");

		String userName;

		Object principal = SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			userName = ((UserDetails)principal).getUsername();
		} else {
			userName = principal.toString();
		}

		LOG.trace("End getPrincipal()");

		return userName;
	}

	@ModelAttribute("roles")
	public List<UserProfile> initializeProfiles() {

		LOG.trace("Init initializeProfiles()");

		return userProfileService.findAll();
	}
}
